<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\FacilityCategory;
use App\Entity\Mitra;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController
 * @package App\Controller
 */
class BaseController extends AbstractController
{
    protected $em;

    protected $methodInput = [];

    protected $data = [];

    protected $fields = [];

    protected $secondFieldstoAdd = [];

    public $tableFields = [];

    public $tableActions = [];

    public $requiredWhere;

    public $requiredWhereParam;

    public $controllerName;

    public $singleNamespace;

    public $class;

    public $secondClassToAdd;

    public $fieldReferenceOnUpdate;

    /** @var Request */
    protected $req;

    private $targetEntity;

    protected $redirectAction;

    protected $parameterRedirect;

    public $formatDTResult = false;

    protected $entity;

    public $notifyUpload;

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @param $template
     *
     * @return Response
     */
    public function renderTable($template)
    {
        switch ($this->singleNamespace) {
            case 'Admin':
                /** @var Admin $query */
                $this->data['avatar'] = $this->getUser()->getAdminProfile()->getAvatar();
                break;
            case 'Mitra' :
                $query = $this->em->getRepository(Mitra::class)->findOneBy(['userId' => $this->getUser()]);
                $this->data['avatar'] = $query->getAvatar();
                break;
            default:
                break;
        }
        $this->data['class'] = $this->controllerName;
        $this->data['space'] = $this->singleNamespace;
        $this->data['tableFields'] = $this->tableFields;
        $this->data['tableAction'] = implode(',', $this->tableActions);
        $this->data['where'] = isset($this->data['where']) ? json_encode($this->data['where']) : null;
        return $this->render(
            $template,
            $this->data
        );

    }

    /**
     * @return RedirectResponse
     */
    public function post()
    {
        try {
            $fields = $this->fields;
            /** @var FacilityCategory $entity */
            $entity = new $this->class();

            foreach ($fields as $field) {
                $finder = explode(':', $field);
                $value = $this->findValue($finder, $entity);
                if (is_array($value) && (isset($value['resetEntity']))) {
                    $entity = $value['class'];
                } else {
                    $setter = 'set' . ucfirst($finder[0]);

                    $entity->$setter($value);
                }
            }
            if (method_exists($entity, 'setAuthor')) {
                $entity->setAuthor($this->getUser());
            }

            if ($this->secondClassToAdd !== null) {
                $sEntity = new $this->secondClassToAdd();
                foreach ($this->secondFieldstoAdd as $field) {
                    $finder = explode(':', $field);
                    if (isset($finder[1]) && $finder[1] === 'PrevToMe') {
                        $setter = 'set' . ucfirst($finder[0]);
                        $entity->$setter($sEntity);
                    } else {
                        if (isset($finder[1]) && $finder[1] === 'relationPrev') {
                            $value = $entity;
                        } else {
                            $value = $this->findValue($finder);
                        }
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    }
                }
                $this->em->persist($sEntity);
            }
            $this->em->persist($entity);

            $this->em->flush();
            $this->addFlash('success', 'Berhasil ditambahkan');

        } catch (\Exception $e) {
            dump($e);
            die();
            $this->addFlash('danger', 'Periksa data, gagal ditambahkan ' . $e->getMessage());

        }

        return $this->redirectToRoute($this->redirectAction);
    }

    /**
     * @return RedirectResponse
     */
    public function edit()
    {
        try {
            $fields = $this->fields;
            $this->entity = new $this->class();

            /** @var FacilityCategory $entity */
            if (method_exists($this->entity, 'setAuthor')) {
                $this->entity = $this->em->getRepository($this->class)->findOneBy(
                    [
                        'id' => $this->req->get('id'),
                        'author' => $this->getUser(),
                    ]
                );
            } else {
                $this->entity = $this->em->getRepository($this->class)->findOneBy(
                    [
                        'id' => $this->req->get('id'),
                    ]
                );
            }
            if ($this->entity === null) {
                $this->addFlash('danger', 'Akses ditolak');
                if ($this->parameterRedirect !== null) {
                    return $this->redirectToRoute($this->redirectAction, $this->parameterRedirect);
                } else {
                    return $this->redirectToRoute($this->redirectAction);
                }
            }


            foreach ($fields as $field) {
                $finder = explode(':', $field);
                $value = $this->findValue($finder, $this->entity);
                if (is_array($value) && (isset($value['resetEntity']))) {
                    $this->entity = $value['class'];
                } else {
                    $setter = 'set' . ucfirst($finder[0]);

                    $this->entity->$setter($value);
                }
            }

            if (method_exists($this->entity, 'setAuthor')) {
                $this->entity->setAuthor($this->getUser());
            }
            if ($this->secondClassToAdd !== null) {
                $getter = 'get' . $this->fieldReferenceOnUpdate;
                $sEntity = $this->entity->$getter();
                foreach ($this->secondFieldstoAdd as $field) {
                    $finder = explode(':', $field);

                    if (!isset($finder[1])) {
                        $value = $this->findValue($finder);
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    } else {
                        $value = $this->findValue($finder);
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    }
                }

            }
//            dump($sEntity);die();
            $this->em->flush();
            $this->addFlash('success', 'Berhasil dirubah');

        } catch (\Exception $e) {
            dump($e);
            die();
            $this->addFlash('danger', $e->getMessage());

        }
//        dump($this->parameterRedirect);die();
        if ($this->parameterRedirect !== null) {
            return $this->redirectToRoute($this->redirectAction, $this->parameterRedirect);
        } else {
            return $this->redirectToRoute($this->redirectAction);
        }
    }

    /**
     * @param $finder
     *
     * @param null $entity
     * @return mixed|object|string|string[]|null
     */
    private function findValue($finder, $entity = null)
    {
        if (isset($finder[1])) {
            if ($finder[1] === 'auto') {
                return str_replace(' ', '-', $this->req->get($finder[2])) . '-' . strtotime("now");
            }

            if ($finder[1] === 'relation') {
                $val = $this->req->get($finder[0]);
                return $this->em->getRepository($finder[2])->findOneBy(['id' => $val]);
            }

            if ($finder[1] === 'relationMTM') {
                $val = $this->req->get($finder[0]);
                $data = $this->em->getRepository($finder[2])->findBy(['id' => $val]);
                $remover = 'remove' . ucfirst($finder[0]);
                $setter = 'add' . ucfirst($finder[0]);
                $getter = 'get' . ucfirst($finder[0]);
                $existingData = $entity->$getter()->toArray();
                foreach ($existingData as $singleData) {
                    $this->entity->$remover($singleData);
                }
                foreach ($data as $single) {
                    $entity->$setter($single);
                }
                return ['resetEntity' => true, 'class' => $entity];
            }

            if ($finder[1] === 'method') {
                $method = $finder[2];
                return $this->$method($finder[3] === 'entity' ? $entity : $finder[3]);
            }
        }

        return $this->req->get($finder[0]);

    }

}